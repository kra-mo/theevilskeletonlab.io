---
layout: post
title: "Response to “Developers are lazy, thus Flatpak”"
description: "Recently, the article “[Developers are lazy, thus Flatpak](https://blog.brixit.nl/developers-are-lazy-thus-flatpak)”, by Martijn Braam, was published to criticize a few things regarding Flatpak. I want to go over the article and address some points that were raised.


While Martijn, the author, contrasted Flatpak with Alpine Linux, I'm going to be contrasting Flatpak with popular Linux distributions, as, to me, it makes sense to contrast Flatpak with some of the most used distributions.


I recommend reading his article before my response, as I won't be replying to every point raised.
"
tags: ["Flatpak"]
toc: true
---

## "Flatpak is a distribution"

> While the developers like to pretend real hard that Flatpak is not a distribution, it's still suspiciously close to one. It lacks a kernel and a few services and it lacks the standard Linux base directory specification but it's still a distribution you need to target. Instead of providing seperate \[sic\] packages with a package manager it provides a runtime that comes with a bunch of dependencies. Conveniently it also provides multiple runtimes to make sure there's not actually a single base to work on. Because sometimes you need Gnome libraries, sometimes you need KDE libraries. Since there's no package manager those will be in seperate \[sic\] runtimes.

I'm not really sure who denies that Flatpak is a distribution, but I'm certainly not one of them. If anything, this seems to be a really good thing to me, because we're bundling a distribution and deploying it to users' systems, meaning that they're getting the exact same builds that we, the developers, tested against. It makes it even easier for us to troubleshoot, because the environment is near-identical.

To quote from Flatpak's [documentation](https://docs.flatpak.org/en/latest/introduction.html): "*Flatpak is a framework for distributing desktop applications across various Linux distributions.*" Even Flatpak agrees that it's a distribution.

## "No built in package manager"

> If you need a dependency that's not in the runtime there's no package manager to pull in that dependency. The solution is to also package the dependencies you need yourself and let the flatpak tooling build this into the flatpak of your application. So now instead of being the developer for your application you're also the maintainer of all the dependencies in this semi-distribution you're shipping under the disguise of an application.

Separating every dependency would be really inconvenient, just like managing graphical apps in a Linux distribution. If, for example, a flatpak named XYZ needs 10 dependencies, whereby one of the dependencies changes the version that the developers of the XYZ flatpak have not tested, then it could lead to bugs or even breakages that would make it really difficult for developers to trace back, as they would need to frequently contact each packager and figure out together. So, as mentioned by the author, the solution is to let the developers bundle everything, as they test their apps against an environment that is easier to understand.

Separating all dependencies is easier for smaller programs and dependencies, as they have a small volume of dependencies, and thus are not as difficult for troubleshooting. However, Flatpak targets graphical apps, which are typically large and sophisticated and need a model that can scale well. In this case, letting developers manage the dependencies is a better approach, as it's easier to maintain and troubleshoot.[^1]

> And one thing is for sure, I don't trust application developers to maintain dependencies.

That's completely fair, as you're entitled to your own opinion. Likewise, I don't trust the worryingly large amount of non-programmers/developers that package dependencies in larger distributions, especially when they have no real world experience in developing apps. Furthermore, I trust even less the package managers that allow dependencies to self-destruct in case [something goes wrong](https://memoryfile.codeberg.page/posts/Distribution-packaging-for-Linux-desktop-applications-is-unsustainable/#steam-on-pop_os).

> This gets really nuts by looking at some software that deals with multimedia. Lets \[sic\] look at the Audacity flatpak. It builds as dependency:
> 
> - wxwidgets
> - ffmpeg
> - sqlite
> - chrpath
> - portaudio
> - portmidi
> 
> So lets \[sic\] look at how well dependencies are managed here. Since we're now almost exactly half a year into 2023 I'll look at the updates for the last 6 months and compare it to the same dependencies in Alpine Linux.
> 
> - audacity has been updated 4 times in the flatpak. It has been updated 5 times on Alpine.
> - ffmpeg has been updated to 6.0 in both the flatpak and Alpine, but the ffmpeg package has had 9 updates because if \[sic\] codecs that have been updated.
> - sqlite hasn't been updated in the flatpak and has been updated 4 times in Alpine
> - wxwidgets hasn't been updated in the flatpak and has been updated 2 times in Alpine
> - chrpath hasn't had updates
> - portaudio hasn't had updates in flatpak and Alpine.
> - portmidi hasn't had updates
> 
> This is just a random package I picked and it already had a lot more maintainance of the dependencies than the flatpak has. It most likely doesn't scale to have all developers keep track of all the dependencies of all their software.

The main issue here is that Audacity has many technical limitations. To name one, in [Audacity's source code](https://github.com/audacity/audacity/blob/d895438a7a153b30cf167e6889f171394cdf7fe1/BUILDING.md), it's documented that "*Audacity on **Linux** uses vanilla version of wxWidgets, we **require** that version **3.1.3** is used. This version is not available in most of the distributions.*"

This is why wxWidgets in the Audacity flatpak wasn't updated. If they updated to major versions, then they might have ran into issues. Alpine Linux, on the other hand, packages wxWidgets version [3.2.2.1](https://web.archive.org/web/20230604034343/https://pkgs.alpinelinux.org/packages?name=wxwidgets&branch=edge&repo=&arch=&maintainer=), which, as seen above, is outside of the "required" version.

Even then, in this case, the amount of updates doesn't signify which is better maintained, as context absolutely matters. It is best to consult the maintainer of the flatpak about their decision, rather than cherry picking and misinforming yourself without doing thorough investigations.

## "The idea of isolation"

This whole section is criticizing GNOME Software, not Flatpak itself. GNOME Software is a frontend that decides how they should display flatpaks to users. If the GNOME Software developers wanted, they could display a dialog that warns the user when they try to install the app. In this case, they went against it and put it under a separate button. In my opinion, it is really unfair to blame a backend utility when the frontend does it "wrong".[^2]

## "So what about traditional distributions"
> I've heard many argument for Flatpaks by users and developers but in the end I can't really say the pros outweigh the cons.
> 
> I think it's very important that developers do not have the permissions to push whatever code they want to everyone under the disguise of a secure system. *And that's my opinion as a software developer*.
> 
> Software packaged by distributions has at least some degree of scrutiny and it often results in at least making sure build flags are set to disable user tracking and such features.

As user [u/natermer](https://www.reddit.com/r/linux/comments/yzvqry/in_defense_of_traditional_packaging/ix2evov/?context=3) explains really well on Reddit: "*the reviewing and security benefits of distribution packaging is \[sic\] heavily overstated by many people defending it. Only a small number of high profile packages ever receive serious security scrutiny. For the most part the packaging authors put in just enough to \[sic\] work to get the software to build correctly and the dependencies to install without breaking anything and then it is up to the end users to report on any problems that crop up. Which means that the guarantees are not really much better then \[sic\] with stuff like "pip".*"

In the Audacity example discussed above, I mentioned that Alpine Linux packages wxWidgets version [3.2.2.1](https://web.archive.org/web/20230604034343/https://pkgs.alpinelinux.org/packages?name=wxwidgets&branch=edge&repo=&arch=&maintainer=) and Audacity, which Audacity has explicitly stated to use 3.1.3. This is a good example of the packaging authors putting in just enough work to get the software to build. The app launching isn't enough to conclude that it works well. While the author's point was regarding security, I think that scrutiny beyond security is equally as important and should be mentioned.

Furthermore, this isn't only about scrutiny, but also maintenance, or the lack thereof. Let's look at the amount of packages that some popular distributions contain in their repositories. As of writing this article:
- Debian has over [1,200 orphaned packages](https://web.archive.org/web/20230425093359/https://www.debian.org/devel/wnpp/orphaned) (no maintainer), many of which are over 1,000 days!
- Fedora Linux has over [800 orphaned packages](https://packager-dashboard.fedoraproject.org/dashboard?users=orphan) (no maintainer).
- Arch Linux has over [400 packages flagged as outdated](https://web.archive.org/web/20230604023515/https://archlinux.org/packages/?sort=&q=&maintainer=&flagged=Flagged).

So, while the author is concerned that developers will "mishandle" dependencies with Flatpak, we observe that the more worrying bit is the amount of unmaintained packages on the distributions you run on your system; packages that are installed *on your host*. So you either choose a mishandled package on your host, or a mishandled dependency inside a container. I will happily take the latter.

> I also believe software in general is better if it's made with the expectation that it will run outside of Flatpak. It's not that hard to make sure you don't depend on bleeding edge versions of libraries while that's not needed. It's not that hard to have optional dependencies in software. It's not that hard to actually follow XDG specifications instead of hardcoding paths.

I've written "[Traditional Packaging is not Suitable for Modern Applications]({% post_url 2022-08-29-traditional-packaging-is-not-suitable-for-modern-applications %})" which goes in depth with the problems of the lack of robust dependency management. To summarize, with traditional packaging systems, developers cannot cherry pick dependencies, and distributions cannot provide consistent experiences and environments if they're not making use of the same containers or similar.

Audacity, as an example, on a traditional packaging system, if the distribution chooses to use the latest version of wxWidgets, [3.2.2.1](https://github.com/wxWidgets/wxWidgets/releases/tag/v3.2.2.1), and the distribution ships that version, then the packagers cannot cherry pick 3.1.3 or any other version, otherwise that version will conflict with the existing version.

This isn't only regarding versioning, but it goes as far as applying custom patches. For example, as taken from my article I linked, [OBS Studio requires a few patches for ffmpeg](https://github.com/obsproject/obs-studio/blob/fe889ec28ebd2f323b5933b7b11c5a9207539c59/CI/flatpak/com.obsproject.Studio.json#L259-L261) to neatly integrate with OBS Studio. This, again, cannot be done if the distribution ships an unpatched ffmpeg or one that doesn't have the patches OBS Studio requires to work properly, otherwise they have to find all sorts of workarounds (refer to my article).

Another problem that traditional packaging systems cannot solve is providing a consistent and predictable environment. For example, Bottles is a "fragile" piece of software, because it needs an environment for Wine and other utilities to run in secure, contained and predictable environments. I've written a [long comment](https://github.com/bottlesdevs/Bottles/issues/2345#issuecomment-1574284415) that explains why supporting traditional packaging systems is a burden for us, while being infeasible at best.

Steam, another example, uses [steam-runtime-tools], which uses [bubblewrap], a utility that originated from Flatpak, to contain and isolate games. Even, though, Steam is available and "supported" on many distributions, they all originate from the same archive, which means that they're all the same binaries and thus are somewhat consistent, just like Flatpak.

As [Linus Torvalds](https://youtu.be/Pzl1B7nB9Kc?t=328) said, "*\[…\] I guarantee you Valve will not make 15 different binaries \[…\]*" --- In reality, developers who have other things to worry about cannot spend their time to build their software 5 times for 5 different Linux distributions and continuously support them. To be fair, ["5" is an understatement](https://upload.wikimedia.org/wikipedia/commons/8/8c/Linux_Distribution_Timeline_Dec._2020.svg). Instead, they want one binary, and continuously and thoroughly test that binary and ship it to users.

## "But packaging for distributions is hard"

> That's the best thing! Developers are not supposed to be the ones packaging software so it's not hard at all. It's not your task to get your software in all the distributions, if your software is useful to people it tends to get pulled in. I have software that's packaged in Alpine Linux, ALT Linux, Archlinux AUR, Debian, Devuan, Fedora, Gentoo, Kali, LiGurOS, Nix, OpenMandriva, postmarketOS, Raspbian, Rosa, Trisquel, Ubuntu and Void. I did not have to package most of this.
> 
> The most I notice from other distributions packaging my software is patches from maintainers that improve the software, usually in dealing with some edge case I forgot with a hardcoded path somewhere.
> 
> The most time I've ever spent on distribution packaging is actually the few pieces of software I've managed to push to Flathub. Dealing with differences between distributions is easy, dealing with differences between runing \[sic\] inside and outside Flatpak is hard.

Masochism aside, as the author wrote in the beginning of the article, "\[s\]adly there's reality". That reality is that developers do not and literally cannot deal with all the burden that distributions cause. They want something that is easy for them to maintain, while having the control to bundle everything they want, and however they see fit, because developers know how their programs work the best. They can test their apps in environments and ship the same environments to users.

If the distribution does not put an effort to make it easy for developers to package, test and ship their apps to users, then the distribution has failed to appeal the majority of developers. The decreased difficulty that Flatpak and Flathub offer is precisely why many distributions are starting to include and use Flathub by default, like Steam OS; it's why GNOME and KDE have been focusing on Flatpak as the primary distribution method; and it's also why Flatpak and Flathub have grown in popularity really quickly.

## "But Flatpaks are easier for end users"

> A second issue I've had on my Pinebook Pro is that it has a 64GB rootfs. Using too many flatpaks is just very wasteful of space. In theory you have a runtime that has your major dependencies and then a few Megabytes of stuff in your application flatpak. In practice I nearly have an \[sic\] unique platform per flatpak installed because the flatpaks depend on different versions of that platform or just on different platforms.

While I have a 256 GB SSD, I probably have more graphical apps than the average user, because I test and contribute to several apps. I would go as much as saying that my Flatpak setup is cursed, but I digress.

Many of the apps use different runtimes and different versions of runtimes. I believe that I fall into the "I nearly have a unique platform per flatpak installed" category, although I believe his statement was hyperbolic.

First, we'll check the amount of storage all flatpaks take. I'll be using [flatpak-dedup-checker] to measure the storage usage:

```terminal
$ ./flatpak-dedup-checker --user
Directories:                /var/home/TheEvilSkeleton/.local/share/flatpak/{runtime,app}
Size without deduplication: 89.31 GB
Size with deduplication:    37.73 GB (42% of 89.31 GB)
Size with compression:      27.66 GB (30% of 89.31 GB. 73% of 37.73 GB)
```

We notice that all flatpaks, without compression, take 37.73 GB in total. Let's look at how many apps I have installed:

```terminal
$ flatpak list --app --user | wc -l
173
```

173 *graphical* apps --- including major browsers, such as Firefox, LibreWolf, Tor Browser, Chromium, ungoogled-chromium, Google Chrome, Brave, Microsoft Edge, and Epiphany. If you're curious, feel free to look at all my installed apps:

```terminal
$ flatpak list --app --user
Name                                         Application ID                                                Version                                Branch                    Origin
Dialect                                      app.drey.Dialect                                              2.1.1                                  stable                    flathub
Elastic                                      app.drey.Elastic                                              0.1.3                                  stable                    flathub
Cambalache                                   ar.xjuan.Cambalache                                           0.10.3                                 stable                    flathub
Valent                                       ca.andyholmes.Valent                                                                                 master                    valent-origin
Dconf Editor                                 ca.desrt.dconf-editor                                         43.0                                   stable                    flathub
Decoder                                      com.belmoussaoui.Decoder                                      0.3.3                                  stable                    flathub
ASHPD Demo                                   com.belmoussaoui.ashpd.demo                                   0.3.0                                  stable                    flathub
Bitwarden                                    com.bitwarden.desktop                                         2023.5.0                               stable                    flathub
Boxy SVG                                     com.boxy_svg.BoxySVG                                          3.96.0                                 stable                    flathub
Brave Browser                                com.brave.Browser                                             1.52.117                               stable                    flathub
Tally for Plausible                          com.cassidyjames.plausible                                    3.0.1                                  stable                    flathub
Discord Canary                               com.discordapp.DiscordCanary                                  0.0.156                                beta                      flathub-beta
Mindustry                                    com.github.Anuken.Mindustry                                   144.3                                  stable                    flathub
ungoogled-chromium                           com.github.Eloston.UngoogledChromium                          113.0.5672.126                         stable                    flathub
Gradience                                    com.github.GradienceTeam.Gradience                            0.4.1                                  stable                    flathub
Desktop Files Creator                        com.github.alexkdeveloper.desktop-files-creator               1.2.2                                  stable                    flathub
Eyedropper                                   com.github.finefindus.eyedropper                              0.6.0                                  stable                    flathub
Rnote                                        com.github.flxzt.rnote                                        0.6.0                                  stable                    flathub
Wike                                         com.github.hugolabe.Wike                                      2.0.1                                  stable                    flathub
Text Pieces                                  com.github.liferooter.textpieces                              3.4.1                                  stable                    flathub
Tor Browser Launcher                         com.github.micahflee.torbrowser-launcher                      0.3.6                                  stable                    flathub
G4Music                                      com.github.neithern.g4music                                   1.13                                   stable                    flathub
Czkawka                                      com.github.qarmin.czkawka                                     5.1.0                                  stable                    flathub
Clapper                                      com.github.rafostar.Clapper                                   0.5.2                                  stable                    flathub
Logisim-evolution                            com.github.reds.LogisimEvolution                              3.8.0                                  stable                    flathub
Avvie                                        com.github.taiko2k.avvie                                      2.3                                    stable                    flathub
Flatseal                                     com.github.tchx84.Flatseal                                    2.0.1                                  stable                    flathub
Frog                                         com.github.tenderowl.frog                                     1.3.0                                  stable                    flathub
Video Downloader                             com.github.unrud.VideoDownloader                              0.12.4                                 stable                    flathub
Easy Effects                                 com.github.wwmm.easyeffects                                   7.0.4                                  stable                    flathub
NewsFlash                                    com.gitlab.newsflash                                          2.3.0                                  stable                    flathub
Google Chrome                                com.google.Chrome                                             113.0.5672.126-1                       stable                    flathub
Inochi Creator                               com.inochi2d.inochi-creator                                   0.8.0                                  stable                    flathub
qView                                        com.interversehq.qView                                        5.0                                    stable                    flathub
GtkStressTesting                             com.leinardi.gst                                              0.7.5                                  stable                    flathub
Forge Sparks                                 com.mardojai.ForgeSparks                                      0.1.1                                  stable                    flathub
Extension Manager                            com.mattjakeman.ExtensionManager                              0.4.0                                  stable                    flathub
Microsoft Edge                               com.microsoft.Edge                                            114.0.1823.37-1                        stable                    flathub
OBS Studio                                   com.obsproject.Studio                                         29.1.2                                 stable                    flathub
Share Preview                                com.rafaelmardojai.SharePreview                               0.3.0                                  stable                    flathub
Black Box                                    com.raggesilver.BlackBox                                      0.13.2                                 stable                    flathub
Geopard                                      com.ranfdev.Geopard                                           1.4.0                                  stable                    flathub
Transmission                                 com.transmissionbt.Transmission                               4.0.3                                  stable                    flathub
Bottles                                      com.usebottles.bottles                                        51.6                                   master                    bottles-origin
Bottles                                      com.usebottles.bottles                                        51.6                                   stable                    flathub
Steam                                        com.valvesoftware.Steam                                       1.0.0.75                               stable                    flathub
Visual Studio Code                           com.visualstudio.code                                         1.78.2-1683731010                      stable                    flathub
Fragments                                    de.haeckerfelix.Fragments                                     2.1.1                                  stable                    flathub
Tubefeeder                                   de.schmidhuberj.tubefeeder                                    v1.9.4                                 master                    tubefeeder-origin
Tubefeeder                                   de.schmidhuberj.tubefeeder                                    v1.9.6                                 stable                    flathub
Tuba                                         dev.geopjr.Tuba                                               0.3.2                                  stable                    flathub
Forecast                                     dev.salaniLeo.forecast                                        0.1.0                                  stable                    flathub
HandBrake                                    fr.handbrake.ghb                                              1.6.1                                  stable                    flathub
Metadata Cleaner                             fr.romainvigier.MetadataCleaner                               2.5.2                                  stable                    flathub
Cartridges                                   hu.kramo.Cartridges                                           1.5.4                                  stable                    flathub
Element                                      im.riot.Riot                                                  1.11.31                                stable                    flathub
Cinny                                        in.cinny.Cinny                                                2.2.6                                  stable                    flathub
Komikku                                      info.febvre.Komikku                                           1.21.1                                 stable                    flathub
Amberol                                      io.bassi.Amberol                                              0.10.3                                 stable                    flathub
Bavarder                                     io.github.Bavarder.Bavarder                                   0.2.3                                  stable                    flathub
AdwSteamGtk                                  io.github.Foldex.AdwSteamGtk                                  0.6.0                                  stable                    flathub
Youtube Downloader Plus                      io.github.aandrew_me.ytdn                                     3.14.0                                 stable                    flathub
Epic Asset Manager                           io.github.achetagames.epic_asset_manager                      3.8.4                                  stable                    flathub
GPU-Viewer                                   io.github.arunsivaramanneo.GPUViewer                          2.26                                   stable                    flathub
Celluloid                                    io.github.celluloid_player.Celluloid                          0.25                                   stable                    flathub
Escambo                                      io.github.cleomenezesjr.Escambo                               0.1.1                                  stable                    flathub
PinApp                                       io.github.fabrialberio.pinapp                                 1.1.7                                  stable                    flathub
Monitorets                                   io.github.jorchube.monitorets                                 0.10.0                                 stable                    flathub
AppImage Pool                                io.github.prateekmedia.appimagepool                           5.1.0                                  stable                    flathub
Kooha                                        io.github.seadve.Kooha                                        2.2.3                                  stable                    flathub
Mousai                                       io.github.seadve.Mousai                                       0.7.5                                  stable                    flathub
WebCord                                      io.github.spacingbat3.webcord                                 4.2.0                                  stable                    flathub
Converter                                    io.gitlab.adhami3310.Converter                                1.6.1                                  stable                    flathub
Sudoku Solver                                io.gitlab.cyberphantom52.sudoku_solver                        1.0.1                                  stable                    flathub
Letterpress                                  io.gitlab.gregorni.ASCIIImages                                1.3.0                                  stable                    flathub
Calligraphy                                  io.gitlab.gregorni.Calligraphy                                1.0.0                                  stable                    flathub
LibreWolf                                    io.gitlab.librewolf-community                                 113.0.2-1                              stable                    flathub
Upscaler                                     io.gitlab.theevilskeleton.Upscaler                                                                   master                    upscaler3-origin
Upscaler                                     io.gitlab.theevilskeleton.Upscaler                            1.1.2                                  stable                    flathub
Upscaler                                     io.gitlab.theevilskeleton.Upscaler                            1.1.2                                  test                      upscaler1-origin
Devel                                        io.gitlab.theevilskeleton.Upscaler.Devel                                                             master                    devel-origin
Dev Toolbox                                  me.iepure.devtoolbox                                          1.0.2                                  stable                    flathub
Passes                                       me.sanchezrodriguez.passes                                    0.7                                    stable                    flathub
Lutris                                       net.lutris.Lutris                                             0.5.13                                 stable                    flathub
Mullvad Browser                              net.mullvad.MullvadBrowser                                    102.9.0esr-12.0-2-build1               stable                    flathub
Poedit                                       net.poedit.Poedit                                             3.3.1                                  stable                    flathub
RPCS3                                        net.rpcs3.RPCS3                                               0.0.28-1-33558d14                      stable                    flathub
Live Captions                                net.sapples.LiveCaptions                                      0.4.0                                  stable                    flathub
Color Picker                                 nl.hjdskes.gcolor3                                            2.4.0                                  stable                    flathub
Audacity                                     org.audacityteam.Audacity                                     3.3.2                                  stable                    flathub
Chromium Web Browser                         org.chromium.Chromium                                         114.0.5735.90                          stable                    flathub
Chromium application base                    org.chromium.Chromium.BaseApp                                                                        21.08                     flathub
Electron2 application base                   org.electronjs.Electron2.BaseApp                                                                     21.08                     flathub
Electron2 application base                   org.electronjs.Electron2.BaseApp                                                                     22.08                     flathub
Flatpak External Data Checker                org.flathub.flatpak-external-data-checker                                                            stable                    flathub
Builder                                      org.flatpak.Builder                                                                                  stable                    flathub
Piper                                        org.freedesktop.Piper                                         0.7                                    stable                    flathub
VulkanInfo                                   org.freedesktop.Platform.VulkanInfo                                                                  22.08                     flathub
appstream-glib                               org.freedesktop.appstream-glib                                0.8.1                                  stable                    flathub
Feeds                                        org.gabmus.gfeeds                                             2.2.0                                  stable                    flathub
Giara                                        org.gabmus.giara                                              1.1.0                                  stable                    flathub
Zola                                         org.getzola.zola                                              0.17.2                                 stable                    flathub
GNU Image Manipulation Program               org.gimp.GIMP                                                 2.99.14                                beta                      flathub-beta
GNU Image Manipulation Program               org.gimp.GIMP                                                 2.10.34                                master                    gnome-nightly
GNU Image Manipulation Program               org.gimp.GIMP                                                 2.10.34                                stable                    flathub
Adwaita Demo                                 org.gnome.Adwaita1.Demo                                       1.4.alpha                              master                    gnome-nightly
Boxes                                        org.gnome.Boxes                                               44.2                                   stable                    flathub
Builder                                      org.gnome.Builder                                             44.2                                   stable                    flathub
Calculator                                   org.gnome.Calculator                                          44.0                                   stable                    flathub
Calendar                                     org.gnome.Calendar                                            44.0                                   stable                    flathub
Contacts                                     org.gnome.Contacts                                            44.0                                   stable                    flathub
Devhelp                                      org.gnome.Devhelp                                             43.0                                   stable                    flathub
Web                                          org.gnome.Epiphany                                            44.3                                   stable                    flathub
File Roller                                  org.gnome.FileRoller                                          43.0                                   stable                    flathub
Firmware                                     org.gnome.Firmware                                            43.2                                   stable                    flathub
Fractal                                      org.gnome.Fractal.Devel                                       5~beta1-c3d77b7                        master                    gnome-nightly
Geary                                        org.gnome.Geary                                               43.0                                   stable                    flathub
Glade                                        org.gnome.Glade                                               3.40.0                                 stable                    flathub
Lollypop                                     org.gnome.Lollypop                                            1.4.37                                 stable                    flathub
Loupe                                        org.gnome.Loupe                                               44.3                                   stable                    flathub
Maps                                         org.gnome.Maps                                                44.2                                   stable                    flathub
Files                                        org.gnome.NautilusDevel                                       44.1                                   master                    gnome-nightly
Notes                                        org.gnome.Notes                                               40.1                                   stable                    flathub
Document Scanner                             org.gnome.SimpleScan                                          44.0                                   stable                    flathub
Text Editor                                  org.gnome.TextEditor                                          44.0                                   stable                    flathub
Endeavour                                    org.gnome.Todo                                                43.0                                   stable                    flathub
Videos                                       org.gnome.Totem                                               43.0                                   stable                    flathub
Weather                                      org.gnome.Weather                                             44.0                                   stable                    flathub
Pika Backup                                  org.gnome.World.PikaBackup                                    0.6.1                                  stable                    flathub
Secrets                                      org.gnome.World.Secrets                                       7.3                                    stable                    flathub
Clocks                                       org.gnome.clocks                                              44.0                                   stable                    flathub
App Icon Preview                             org.gnome.design.AppIconPreview                               3.3.0                                  stable                    flathub
Contrast                                     org.gnome.design.Contrast                                     0.0.8                                  stable                    flathub
Emblem                                       org.gnome.design.Emblem                                       1.2.0                                  stable                    flathub
Icon Library                                 org.gnome.design.IconLibrary                                  0.0.16                                 stable                    flathub
Lorem                                        org.gnome.design.Lorem                                        1.2                                    stable                    flathub
Color Palette                                org.gnome.design.Palette                                      2.0.2                                  stable                    flathub
Symbolic Preview                             org.gnome.design.SymbolicPreview                              0.0.8                                  stable                    flathub
Typography                                   org.gnome.design.Typography                                   0.2.0                                  stable                    flathub
Fonts                                        org.gnome.font-viewer                                         44.0                                   stable                    flathub
Identity                                     org.gnome.gitlab.YaLTeR.Identity                              0.5.0                                  stable                    flathub
Iotas                                        org.gnome.gitlab.cheywood.Iotas                               0.1.16                                 stable                    flathub
Apostrophe                                   org.gnome.gitlab.somas.Apostrophe                             2.6.3                                  stable                    flathub
GTK Demo                                     org.gtk.Demo4                                                                                        master                    gnome-nightly
Inkscape                                     org.inkscape.Inkscape                                         1.2.2                                  stable                    flathub
JDownloader                                  org.jdownloader.JDownloader                                   2.0                                    stable                    flathub
Dolphin                                      org.kde.dolphin                                               23.04.1                                stable                    flathub
Kdenlive                                     org.kde.kdenlive                                              23.04.1                                stable                    flathub
Krita                                        org.kde.krita                                                 5.1.5                                  stable                    flathub
NeoChat                                      org.kde.neochat                                               23.04.1                                stable                    flathub
Xwayland Video Bridge                        org.kde.xwaylandvideobridge                                                                          master                    xwaylandvideobridge-origin
KeePassXC                                    org.keepassxc.KeePassXC                                       2.7.5                                  stable                    flathub
LibreOffice                                  org.libreoffice.LibreOffice                                   7.5.3.2                                stable                    flathub
Thunderbird                                  org.mozilla.Thunderbird                                       102.11.2                               stable                    flathub
Firefox                                      org.mozilla.firefox                                           113.0.2                                stable                    flathub
Tagger                                       org.nickvision.tagger                                         2022.11.2                              stable                    flathub
Nicotine+                                    org.nicotine_plus.Nicotine                                    3.2.9                                  stable                    flathub
ONLYOFFICE Desktop Editors                   org.onlyoffice.desktopeditors                                 7.3.3                                  stable                    flathub
Helvum                                       org.pipewire.Helvum                                           0.4.0                                  stable                    flathub
qBittorrent                                  org.qbittorrent.qBittorrent                                   4.5.3                                  stable                    flathub
Tenacity                                     org.tenacityaudio.Tenacity                                    1.3-beta3                              test                      tenacity-origin
Wine                                         org.winehq.Wine                                               7.0                                    stable-21.08              flathub
Wine                                         org.winehq.Wine                                               8.0                                    stable-22.08              flathub
Zrythm                                       org.zrythm.Zrythm                                             1.0.0-beta.4.9.1                       stable                    flathub
Imaginer                                     page.codeberg.Imaginer.Imaginer                               0.2.2                                  stable                    flathub
Atoms                                        pm.mirko.Atoms                                                1.1.1                                  stable                    flathub
Commit                                       re.sonny.Commit                                               4.0                                    stable                    flathub
Oh My SVG                                    re.sonny.OhMySVG                                              1.2                                    stable                    flathub
Playhouse                                    re.sonny.Playhouse                                            1.1                                    stable                    flathub
Workbench                                    re.sonny.Workbench                                            44.1                                   stable                    flathub
Graphs                                       se.sjoerd.Graphs                                              1.5.2                                  stable                    flathub
Cawbird                                      uk.co.ibboard.cawbird                                         1.5                                    stable                    flathub
ArmCord                                      xyz.armcord.ArmCord                                           3.2.0                                  stable                    flathub
```

Alright, let's look at the amount of runtimes installed:

```terminal
$ flatpak list --runtime --user | wc -l
97
```

And the runtimes themselves:

```terminal
$ flatpak list --runtime --user
Name                                                     Application ID                                                 Version                                      Branch                 Origin
Codecs                                                   com.github.Eloston.UngoogledChromium.Codecs                                                                 stable                 flathub
Proton (community build)                                 com.valvesoftware.Steam.CompatibilityTool.Proton               7.0-6                                        beta                   flathub-beta
Proton (community build)                                 com.valvesoftware.Steam.CompatibilityTool.Proton               7.0-5                                        stable                 flathub
Proton experimental (community build)                    com.valvesoftware.Steam.CompatibilityTool.Proton-Exp           7.0-20230208                                 stable                 flathub
Proton-GE (community build)                              com.valvesoftware.Steam.CompatibilityTool.Proton-GE            8.3-1                                        beta                   flathub-beta
Proton-GE (community build)                              com.valvesoftware.Steam.CompatibilityTool.Proton-GE            8.3-1                                        stable                 flathub
gamescope                                                com.valvesoftware.Steam.Utility.gamescope                      3.11.51                                      stable                 flathub
Codecs                                                   org.audacityteam.Audacity.Codecs                                                                            stable                 flathub
Codecs                                                   org.chromium.Chromium.Codecs                                                                                stable                 flathub
Calf                                                     org.freedesktop.LinuxAudio.Plugins.Calf                        0.90.3                                       22.08                  flathub
LSP                                                      org.freedesktop.LinuxAudio.Plugins.LSP                         1.2.6                                        22.08                  flathub
MDA                                                      org.freedesktop.LinuxAudio.Plugins.MDA                         1.2.10                                       22.08                  flathub
TAP-plugins                                              org.freedesktop.LinuxAudio.Plugins.TAP                         1.0.1                                        22.08                  flathub
ZamPlugins                                               org.freedesktop.LinuxAudio.Plugins.ZamPlugins                  4.1                                          22.08                  flathub
SWH                                                      org.freedesktop.LinuxAudio.Plugins.swh                         0.4.17                                       22.08                  flathub
Freedesktop Platform                                     org.freedesktop.Platform                                       21.08.18                                     21.08                  flathub
Freedesktop Platform                                     org.freedesktop.Platform                                       22.08.12.1                                   22.08                  flathub
i386                                                     org.freedesktop.Platform.Compat.i386                                                                        21.08                  flathub
i386                                                     org.freedesktop.Platform.Compat.i386                                                                        22.08                  flathub
Mesa                                                     org.freedesktop.Platform.GL.default                            21.3.9                                       21.08                  flathub
Mesa                                                     org.freedesktop.Platform.GL.default                            23.1.1                                       22.08                  flathub
Mesa (Extra)                                             org.freedesktop.Platform.GL.default                            23.1.1                                       22.08-extra            flathub
Mesa git snapshot                                        org.freedesktop.Platform.GL.mesa-git                           23.0-branchpoint-4408-g4ac56e3e5a4           23.08beta              flathub-beta
default                                                  org.freedesktop.Platform.GL32.default                                                                       21.08                  flathub
Mesa                                                     org.freedesktop.Platform.GL32.default                          23.1.1                                       22.08                  flathub
Mesa (Extra)                                             org.freedesktop.Platform.GL32.default                          23.1.1                                       22.08-extra            flathub
Mesa git snapshot                                        org.freedesktop.Platform.GL32.mesa-git                         23.0-branchpoint-4408-g4ac56e3e5a4           23.08beta              flathub-beta
MangoHud                                                 org.freedesktop.Platform.VulkanLayer.MangoHud                  0.6.9-1                                      22.08                  flathub
vkBasalt                                                 org.freedesktop.Platform.VulkanLayer.vkBasalt                  0.3.2.9                                      22.08                  flathub
ffmpeg-full                                              org.freedesktop.Platform.ffmpeg-full                                                                        21.08                  flathub
ffmpeg-full                                              org.freedesktop.Platform.ffmpeg-full                                                                        22.08                  flathub
i386                                                     org.freedesktop.Platform.ffmpeg_full.i386                                                                   21.08                  flathub
i386                                                     org.freedesktop.Platform.ffmpeg_full.i386                                                                   22.08                  flathub
openh264                                                 org.freedesktop.Platform.openh264                              2.1.0                                        2.0                    flathub
openh264                                                 org.freedesktop.Platform.openh264                              2.1.0                                        2.0beta                flathub-beta
openh264                                                 org.freedesktop.Platform.openh264                              2.1.0                                        2.2.0                  flathub
openh264                                                 org.freedesktop.Platform.openh264                              2.1.0                                        2.2.0beta              gnome-nightly
Freedesktop SDK                                          org.freedesktop.Sdk                                            21.08.18                                     21.08                  flathub
Freedesktop SDK                                          org.freedesktop.Sdk                                            22.08.12.1                                   22.08                  flathub
i386                                                     org.freedesktop.Sdk.Compat.i386                                                                             21.08                  flathub
i386                                                     org.freedesktop.Sdk.Compat.i386                                                                             22.08                  flathub
.NET Core SDK extension                                  org.freedesktop.Sdk.Extension.dotnet6                          6.0.408                                      21.08                  flathub
Free Pascal Compiler and Lazarus                         org.freedesktop.Sdk.Extension.freepascal                       3.2.2                                        21.08                  flathub
Go programming language Sdk extension                    org.freedesktop.Sdk.Extension.golang                           1.20.2                                       21.08                  flathub
OpenJDK 11 SDK Extension                                 org.freedesktop.Sdk.Extension.openjdk11                                                                     21.08                  flathub
OpenJDK 17 SDK Extension                                 org.freedesktop.Sdk.Extension.openjdk17                                                                     22.08                  flathub
Rust stable                                              org.freedesktop.Sdk.Extension.rust-stable                      1.67.0                                       21.08                  flathub
Rust stable                                              org.freedesktop.Sdk.Extension.rust-stable                      1.70.0                                       22.08                  flathub
toolchain-i386                                           org.freedesktop.Sdk.Extension.toolchain-i386                                                                21.08                  flathub
toolchain-i386                                           org.freedesktop.Sdk.Extension.toolchain-i386                                                                22.08                  flathub
toolchain-i386                                           org.freedesktop.Sdk.Extension.toolchain-i386                                                                22.08beta              flathub-beta
GNOME Boxes Osinfo DB                                    org.gnome.Boxes.Extension.OsinfoDb                             20230518                                     stable                 flathub
HEIC                                                     org.gnome.Loupe.HEIC                                                                                        stable                 flathub
GNOME Application Platform version 41                    org.gnome.Platform                                                                                          41                     flathub
GNOME Application Platform version 42                    org.gnome.Platform                                                                                          42                     flathub
GNOME Application Platform version 43                    org.gnome.Platform                                                                                          43                     flathub
GNOME Application Platform version 44                    org.gnome.Platform                                                                                          44                     flathub
GNOME Application Platform version Nightly               org.gnome.Platform                                                                                          master                 gnome-nightly
i386                                                     org.gnome.Platform.Compat.i386                                                                              41                     flathub
i386                                                     org.gnome.Platform.Compat.i386                                                                              43                     flathub
i386                                                     org.gnome.Platform.Compat.i386                                                                              44                     flathub
GNOME Software Development Kit version 41                org.gnome.Sdk                                                                                               41                     flathub
GNOME Software Development Kit version 42                org.gnome.Sdk                                                                                               42                     flathub
GNOME Software Development Kit version 43                org.gnome.Sdk                                                                                               43                     flathub
GNOME Software Development Kit version 44                org.gnome.Sdk                                                                                               44                     flathub
GNOME Software Development Kit version Nightly           org.gnome.Sdk                                                                                               master                 gnome-nightly
i386                                                     org.gnome.Sdk.Compat.i386                                                                                   41                     flathub
i386                                                     org.gnome.Sdk.Compat.i386                                                                                   42                     flathub
i386                                                     org.gnome.Sdk.Compat.i386                                                                                   43                     flathub
i386                                                     org.gnome.Sdk.Compat.i386                                                                                   44                     flathub
i386                                                     org.gnome.Sdk.Compat.i386                                                                                   master                 gnome-nightly
Codecs                                                   org.gnome.Totem.Codecs                                                                                      stable                 flathub
yt-dl totem-pl-parser plugin                             org.gnome.Totem.Videosite.YouTubeDl                                                                         stable                 flathub
Adwaita dark GTK theme                                   org.gtk.Gtk3theme.Adwaita-dark                                                                              3.22                   flathub
adw-gtk3 Gtk Theme                                       org.gtk.Gtk3theme.adw-gtk3                                                                                  3.22                   flathub
adw-gtk3-dark Gtk Theme                                  org.gtk.Gtk3theme.adw-gtk3-dark                                                                             3.22                   flathub
Adwaita theme                                            org.kde.KStyle.Adwaita                                                                                      6.4                    flathub
Kvantum theme engine                                     org.kde.KStyle.Kvantum                                         1.0.6                                        5.15-21.08             flathub
KDE Application Platform                                 org.kde.Platform                                                                                            5.15-21.08             flathub
KDE Application Platform                                 org.kde.Platform                                                                                            5.15-22.08             flathub
KDE Application Platform                                 org.kde.Platform                                                                                            6.4                    flathub
QGnomePlatform                                           org.kde.PlatformTheme.QGnomePlatform                                                                        5.15-21.08             flathub
QGnomePlatform                                           org.kde.PlatformTheme.QGnomePlatform                                                                        5.15-22.08             flathub
QGnomePlatform                                           org.kde.PlatformTheme.QGnomePlatform                                                                        6.4                    flathub
QtSNI                                                    org.kde.PlatformTheme.QtSNI                                                                                 5.15-21.08             flathub
KDE Software Development Kit                             org.kde.Sdk                                                                                                 5.15-21.08             flathub
KDE Software Development Kit                             org.kde.Sdk                                                                                                 6.4                    flathub
QGnomePlatform-decoration                                org.kde.WaylandDecoration.QGnomePlatform-decoration                                                         5.15-21.08             flathub
QGnomePlatform-decoration                                org.kde.WaylandDecoration.QGnomePlatform-decoration                                                         5.15-22.08             flathub
QGnomePlatform-decoration                                org.kde.WaylandDecoration.QGnomePlatform-decoration                                                         6.4                    flathub
Codecs                                                   org.kde.krita.Codecs                                                                                        stable                 flathub
DXVK                                                     org.winehq.Wine.DLLs.dxvk                                      1.10.3                                       stable-21.08           flathub
DXVK                                                     org.winehq.Wine.DLLs.dxvk                                      1.10.3                                       stable-22.08           flathub
Gecko                                                    org.winehq.Wine.gecko                                                                                       stable-21.08           flathub
Gecko                                                    org.winehq.Wine.gecko                                                                                       stable-22.08           flathub
Mono                                                     org.winehq.Wine.mono                                                                                        stable-21.08           flathub
Mono                                                     org.winehq.Wine.mono                                                                                        stable-22.08           flathub
```

In that output, here are some of the interesting bits:

```
Name                                                     Application ID                                                 Version                                      Branch                 Origin

[…]

GNOME Application Platform version 41                    org.gnome.Platform                                                                                          41                     flathub
GNOME Application Platform version 42                    org.gnome.Platform                                                                                          42                     flathub
GNOME Application Platform version 43                    org.gnome.Platform                                                                                          43                     flathub
GNOME Application Platform version 44                    org.gnome.Platform                                                                                          44                     flathub
GNOME Application Platform version Nightly               org.gnome.Platform                                                                                          master                 gnome-nightly

[…]

KDE Application Platform                                 org.kde.Platform                                                                                            5.15-21.08             flathub
KDE Application Platform                                 org.kde.Platform                                                                                            5.15-22.08             flathub
KDE Application Platform                                 org.kde.Platform                                                                                            6.4                    flathub

[…]

DXVK                                                     org.winehq.Wine.DLLs.dxvk                                      1.10.3                                       stable-21.08           flathub
DXVK                                                     org.winehq.Wine.DLLs.dxvk                                      1.10.3                                       stable-22.08           flathub
Gecko                                                    org.winehq.Wine.gecko                                                                                       stable-21.08           flathub
Gecko                                                    org.winehq.Wine.gecko                                                                                       stable-22.08           flathub
Mono                                                     org.winehq.Wine.mono                                                                                        stable-21.08           flathub
Mono                                                     org.winehq.Wine.mono                                                                                        stable-22.08           flathub
```

We can observe that, just like the author, I have many different versions of runtimes. Even with an unusual amount of runtimes and apps, Flatpak somehow manages to use 37.73 GB, *even with most browsers installed as a flatpak*.

I imagine that most users have a small selection of apps installed, which only come with a few runtimes and a version apart, which also means that my setup is possibly one of the worst case scenarios. Even with that amount of torture, Flatpak still manages to handle storage fairly well.

## "Flatpak does have it's \[sic\] uses"

> I wouldn't say Flatpak is completely useless. For certain usecases \[sic\] it is great to have available. It \[sic\] think Flatpak makes most sense for when closed source software would need to be distributed.

This is something that is really important to address: Flatpak (Flathub) makes even *more* sense for free and open-source (FOSS) apps than closed source, because they make apps easily discoverable. For example, [Upscaler], an app I developed as a final assignment in [CS50x] and published it on Flathub on [November 16 2022](https://github.com/flathub/flathub/pull/3648#issuecomment-1316659408), was featured on [OMG! Ubuntu! on that same day](https://www.omgubuntu.co.uk/2022/11/upscaler-open-source-ai-image-upscale-app-for-linux) (hover over the date for the published date). Another example, [gregorni](https://gitlab.gnome.org/gregorni), a friend of mine, published [Calligraphy] on [June 1 2023](https://github.com/flathub/flathub/pull/4194#issuecomment-1571390015), which was featured on [OMG! Linux! on that same day](https://www.omglinux.com/calligraphy-ascii-text-art-linux).

While it makes a lot of sense for closed source apps to publish their apps on Flathub, in my opinion, FOSS apps get even more benefits than closed source apps, because news outlets, especially the FOSS targeted ones, will quickly discover your apps, and might even write an article about them. This also means that more users will discover your apps, which helps it grow in popularity. This *also* means that you're not forced to rely on GitHub to make your app discoverable. You could actually use [Codeberg] and have your apps easily discoverable if they are published on stores that are designed to be discovered.

[Imaginer] and [Bavarder] are some GTK4+libadwaita apps that are primarily available on Codeberg, yet both gain more than 100 downloads a day on average, which is a pretty big achievement in my opinion.

## Conclusion

In the end, I respect the opinion of disliking Flatpak, as we all have different opinions and disagree on many things. However, there is a difference between having an opinion, and being misinformed and displaying them to viewers or readers, especially when it can potentially hurt the community who are working really hard on addressing genuine issues on the Linux desktop.

As an app developer, I cannot predict users' setups; I prefer not to waste my time to research what dependencies and their versions Fedora Linux, Debian, Ubuntu, Arch Linux, etc. package. I prefer not to waste my time to refer users to Bugzilla, mailing lists, IRC or other inconvenient platforms that no one wants to use, and later figure out that they've been packaging a 6 month old version of the app. Instead, I prefer to devote my time on fixing actual bugs, adding new features, tweak some designs, work on other projects, write articles, or, you know, touch grass for once.

---

**Footnotes**

[^1]: Unless it's more practical to collaborate and create [BaseApps](https://docs.flatpak.org/en/latest/introduction.html?highlight=BaseApp#terminology). BaseApps' use case is bundling dependencies that frameworks or the like need, to reduce the amount of duplication effort.
[^2]: In my humble opinion, I prefer GNOME Software's approach as it's less obstructive and doesn't get in my way.

[BaseApp]: https://docs.flatpak.org/en/latest/introduction.html?highlight=BaseApp#terminology
[NVIDIA userspace drivers]: https://github.com/flathub/org.freedesktop.Platform.GL.nvidia
[ffmpeg]: https://github.com/flathub/org.freedesktop.Platform.ffmpeg
[steam-runtime-tools]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools
[bubblewrap]: https://github.com/containers/bubblewrap
[flatpak-dedup-checker]: https://gitlab.com/TheEvilSkeleton/flatpak-dedup-checker
[CS50x]: https://www.edx.org/course/introduction-computer-science-harvardx-cs50x
[Upscaler]: https://flathub.org/apps/io.gitlab.theevilskeleton.Upscaler
[Calligraphy]: https://flathub.org/apps/io.gitlab.gregorni.Calligraphy
[Codeberg]: https://codeberg.org
[Imaginer]: https://codeberg.org/Imaginer/Imaginer
[Bavarder]: https://codeberg.org/Bavarder/Bavarder
