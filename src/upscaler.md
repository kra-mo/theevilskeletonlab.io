---
title: "Upscaler"
description: "Upscale and enhance images"
layout: general
embed_image: "/assets/upscaler.webp"

presentation:
- title: "Private & Secure"
  image: "/assets/upscaler-is-safe.webp"
  description: "Upscaler is fully restricted and cannot access your files, devices, or even the internet."
- title: "Local-only"
  image: "/assets/upscaler-running-locally.webp"
  description: "There's absolutely no internet access. Everything is run on your personal machine."
- title: "Restore Images"
  image: "/assets/upscaled.webp"
  description: "With just a few clicks, you can turn blurry images into clearer and high-quality images."
---

<center class="big"><p>{{ page.description }}</p></center>

<center><p><a href='https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a></p></center>

<center>
<p><a href="https://matrix.to/#/%23Upscaler%3Agnome.org"><img src="https://img.shields.io/matrix/upscaler:matrix.org" alt="Matrix chat"></a> • <a href="https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler"><img src="https://img.shields.io/flathub/downloads/io.gitlab.theevilskeleton.Upscaler?label=Installs" alt="Installs"></a> • <a href="https://gitlab.gnome.org/World/Upscaler"><img src="https://img.shields.io/gitlab/v/tag/World/Upscaler?gitlab_url=https%3A%2F%2Fgitlab.gnome.org&amp;label=Upscaler" alt="GitLab tag (latest by SemVer)"></a></p>
</center>

{% include image.html
url="/assets/upscaler.webp"
%}

# <u>Features</u>

{% include presentation.html rows=page.presentation %}

## <u>System Requirements</u>

- OS: Linux-based operating system
- Graphics: [Vulkan]-capable GPU

## <u>Technical Details</u>

Upscaler is powered by [Real-ESRGAN ncnn Vulkan], which is the [ncnn] implementation of [Real-ESRGAN], a program that aims to develop practical algorithms for general image/video restoration.

The app is written in Python, using [PyGObject]. It uses [GTK4] and [libadwaita].

## <u>Free and Open-Source Software</u>

Upscaler's source code is released on [GNOME GitLab] under the GPLv3 license. [Real-ESRGAN ncnn Vulkan] is released on [GitHub] under the MIT license.

## <u>Origins of Upscaler</u>

Upscaler started out as a final project for the final [CS50x](https://www.edx.org/course/introduction-computer-science-harvardx-cs50x) assignment, Harvard University's introductory course to computer science. It was later decided to continue it as free and open-source software. Without CS50x, Upscaler wouldn’t exist.

[Vulkan]: https://vulkan.gpuinfo.org/listdevices.php?platform=linux
[GNOME GitLab]: https://gitlab.gnome.org/World/Upscaler
[GitHub]: https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan
[Upscaler Contributors]: https://gitlab.gnome.org/World/Upscaler/-/graphs/main
[Real-ESRGAN ncnn Vulkan]: https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan
[GTK4]: https://docs.gtk.org/gtk4
[libadwaita]: https://gnome.pages.gitlab.gnome.org/libadwaita
[ncnn]: https://github.com/Tencent/ncnn
[Real-ESRGAN]: https://github.com/xinntao/Real-ESRGAN
[PyGObject]: https://pygobject.readthedocs.io/en/latest
